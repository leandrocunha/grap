grap (1.46-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Added debian/upstream/metadata.
  * Added debian/docs.
  * debian/copyright:
    - Added Upstream-Contact optional field according DEP-5.
    - Added myself.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Tue, 26 Jan 2021 03:24:55 -0300

grap (1.45-4) unstable; urgency=medium

  * QA upload.
  * Do not embed the running kernel version in the binaries.
    (Closes: #977609)
  * debian/control: Set Rules-Requires-Root to no.
  * Update to debhelper-compat 13.
  * debian/copyright: Use https URLs.
  * debian/control: Update to Standards-Version 4.5.1.

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Sun, 20 Dec 2020 21:03:15 -0800

grap (1.45-3) unstable; urgency=medium

  * No-change source-only upload to ensure migration to testing. Thanks,
    Axel Beckert for letting me know.

 -- Chris Lamb <lamby@debian.org>  Tue, 22 Sep 2020 10:37:28 +0100

grap (1.45-2) unstable; urgency=medium

  * QA upload.
  * Make the build reproducible. (Closes: #870573)
  * Bump Standards-Version to 4.5.0.
  * Move to HTTPS homepage URL and in debian/watch.

 -- Chris Lamb <lamby@debian.org>  Wed, 16 Sep 2020 10:26:17 +0100

grap (1.45-1) unstable; urgency=medium

  * QA upload.
  * Acknowledge NMU, enabling hardening flags. Closes: #832819
  * Orphan package, set maintainer to Debian QA Group
  * Imported Upstream version 1.45
  * Update watch file
  * Update d/copyright
  * Use debhelper v10
  * Update Standards-Version to 3.9.8, no changes needed
  * Remove patch, the unneeded files are now removed in d/rules

 -- Dr. Tobias Quathamer <toddy@debian.org>  Tue, 22 Nov 2016 13:26:44 +0100

grap (1.44-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop the Build-Depends on hardening-wrapper. (Closes: #836627)

 -- Chris Lamb <lamby@debian.org>  Sun, 25 Sep 2016 12:12:46 +0200

grap (1.44-1) unstable; urgency=low

  * New maintainer. Closes: #615895
  * New Upstream Version
  * Remove override_dh_clean from debian/rules
  * Remove patches, since are not current or fixed in upstream:
    0001-Do-not-remove-too-much-upon-make-distclean.patch
    0003-Add-missing-format-argument-to-snprintf.patch
    0004-Fix-typo-in-manpage.patch
  * New patch 0001-adapt-for-Debian-packaging.patch substitutes previous
    0002-Adapt-for-Debian-packaging.patch
  * Adapting paths in manpage is moved from
    0002-Adapt-for-Debian-packaging.patch to rules file
  * Update Standards-Version to 3.9.5
  * Update debhelper to v9
  * debian/dirs and debian/docs not needed anymore in debhelper v9

 -- Matus Valo <matusvalo@gmail.com>  Tue, 25 Feb 2014 23:47:00 +0100

grap (1.43-2) unstable; urgency=low

  * Orphan package
  * Add missing format argument to snprintf. Closes: #614434
  * Fix typo in manpage
  * Use source format 3.0 (quilt)
  * Switch to debhelper v8
  * Reduce debian/rules
  * Add ${misc:Depends} to Depends
  * Shorten list of debian/dirs
  * Update year in debian/copyright
  * Update Standards-Version to 3.9.1

 -- Tobias Quathamer <toddy@debian.org>  Mon, 28 Feb 2011 22:00:52 +0100

grap (1.43-1) unstable; urgency=low

  * New Upstream Version
  * Remove comments from watch file

 -- Tobias Quathamer <toddy@debian.org>  Sun, 19 Oct 2008 17:59:43 +0200

grap (1.42-1) unstable; urgency=low

  * New Upstream Version
  * Change my last name and adjust copyright years
  * Update Standards-Version to 3.8.0, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Sat, 04 Oct 2008 09:34:46 +0200

grap (1.41-2) unstable; urgency=low

  * Apply patches directly and remove quilt from Build-Depends
  * Build package with hardening-wrapper
  * Change maintainer e-mail address
  * Remove DM upload rights

 -- Tobias Toedter <toddy@debian.org>  Sat, 31 May 2008 18:36:26 +0200

grap (1.41-1) unstable; urgency=low

  * New upstream version
    - Fix a FTBFS bug with GCC 4.3 due to missing #includes.
      Thanks to Martin Michlmayr for the report. Closes: #455161
  * Update Standards-Version to 3.7.3, no changes needed
  * Move Homepage from long description to control field
  * Ignore a missing Makefile but catch all other errors in the
    clean target
  * Update patches to apply cleanly, no content changes
  * Allow Debian Maintainer uploads

 -- Tobias Toedter <t.toedter@gmx.net>  Wed, 26 Dec 2007 18:36:26 +0100

grap (1.40-1) unstable; urgency=low

  * New upstream version
  * Remove script "preinst" to remove the /usr/doc/grap symlink upon
    upgrade or installation. This has been taken care of with etch.
  * Update patches to apply cleanly, no content changes
  * Update copyright file

 -- Tobias Toedter <t.toedter@gmx.net>  Tue, 18 Apr 2007 13:45:09 +0200

grap (1.39-2) unstable; urgency=low

  * Add script preinst to remove the /usr/doc/grap symlink upon
    upgrade or installation. The previous code in prerm failed to do so.
    Thanks to David Frey for spotting this. Closes: #380393
  * Reformat the long description, including the correct format for
    "Homepage:" with a leading space
  * Build-Depends: on quilt, which is now used for managing the patches
  * Update to Standards-Version 3.7.2, no changes needed

 -- Tobias Toedter <t.toedter@gmx.net>  Sun, 30 Jul 2006 18:42:35 +0200

grap (1.39-1) unstable; urgency=low

  * New upstream version
    - Includes a patch to fix a compilation error with GCC 4.1.
      Thanks to Martin Michlmayr for the report and patch. Closes: #357184
    - The typo in the README file is now fixed, making the Debian patch
      unnecessary
  * Adapted other patches for the new source layout
  * Made use of configure options --with-defines-dir and --with-example-dir
    in debian/rules
  * Build-Depends: debhelper >= 5.0.0
  * Bumped debhelper compat file from 4 to 5
  * Updated copyright file

 -- Tobias Toedter <t.toedter@gmx.net>  Mon, 27 Mar 2006 22:27:03 +0200

grap (1.36-2) unstable; urgency=low

  * Removed Makefile from the examples/ directory. It's autogenerated
    and was included in the .diff.gz errorneously, because upstream
    does not remove it in the distclean target
  * New patch 30_Makefile.common.diff which does not remove the
    distributed documentation files in the distclean target
  * Added homepage URL to the description
  * Make sure that the location of the example files and the definition
    file in the man page are correct

 -- Tobias Toedter <t.toedter@gmx.net>  Sun, 11 Dec 2005 13:51:58 +0100

grap (1.36-1) unstable; urgency=low

  * New upstream release
    - Fixed FTBFS with gcc-4.0. Closes: #286911
  * Thanks to Roland Mas for the Non-maintainer upload
    - Fixed compilation problem with recent flex (patch from Joshua Kwan
      <joshk@triplehelix.org>). Closes: #191189
  * New maintainer. Closes: #321335
  * Standards-Version is now 3.6.2, no changes needed
  * Several minor changes:
    - Added debhelper and autotools-dev as Build-Depends:
    - Rewrote debian/rules
    - Removed the script "prerm"
    - Removed the article "A" from the short description

 -- Tobias Toedter <t.toedter@gmx.net>  Mon,  8 Aug 2005 18:13:17 +0200

grap (1.26-4.1) unstable; urgency=low

  * Non-maintainer upload according to the release manager's plan.
  * Fixed compilation problem with recent flex (patch from Joshua Kwan
    <joshk@triplehelix.org>).  (Closes: Bug#191189)

 -- Roland Mas <lolando@debian.org>  Sun, 31 Aug 2003 21:59:08 +0200

grap (1.26-4) unstable; urgency=low

  * Fixed location of defines files in grap source.

 -- Brian Mays <brian@debian.org>  Wed,  8 Jan 2003 12:16:37 -0500

grap (1.26-3) unstable; urgency=low

  * Fixed bug in clipping routine.

 -- Brian Mays <brian@debian.org>  Mon,  6 Jan 2003 23:41:20 -0500

grap (1.26-2) unstable; urgency=low

  * Added missing include.  (Closes: Bug#175386)

 -- Brian Mays <brian@debian.org>  Sat,  4 Jan 2003 23:00:38 -0500

grap (1.26-1) unstable; urgency=low

  * New upstream version.

 -- Brian Mays <brian@debian.org>  Wed, 16 Oct 2002 10:59:43 -0400

grap (1.23-3) unstable; urgency=high

  * Fixed location of grap's examples directory.  (Closes: Bug#159770)

 -- Brian Mays <brian@debian.org>  Thu,  5 Sep 2002 17:42:04 -0400

grap (1.23-2) unstable; urgency=medium

  * Modified postinst to ignore an existing /usr/doc/$package directory.

 -- Brian Mays <brian@debian.org>  Wed, 20 Mar 2002 11:17:19 -0500

grap (1.23-1) unstable; urgency=low

  * New upstream version.

 -- Brian Mays <brian@debian.org>  Wed, 20 Feb 2002 11:02:42 -0500

grap (1.22-2) unstable; urgency=low

  * Fixed build process to include the defines files in the package.

 -- Brian Mays <brian@debian.org>  Thu,  7 Feb 2002 12:08:28 -0500

grap (1.22-1) unstable; urgency=low

  * New upstream version.
  * Fixed symlink /usr/doc -> ../share/doc.
  * Fixed reference to GPL.
  * Improved stripping of binaries.
  * Modified grap to honor ".lf" commands.
  * Fixed misspellings of Brian Kernighan's name.  (Closes: Bug#124701)

 -- Brian Mays <brian@debian.org>  Fri,  1 Feb 2002 15:42:40 -0500

grap (1.21-1) unstable; urgency=low

  * New upstream version.
  * Added patch from LaMont Jones <lamont@smallone.fc.hp.com> to fix
    build on hppa.  (Closes: Bug#111407)

 -- Brian Mays <brian@debian.org>  Mon, 26 Nov 2001 12:00:49 -0500

grap (1.20-3) unstable; urgency=low

  * Added patch to fix problems on ia64 (gcc 2.96).  (Closes: Bug#109285)
  * Changed references to the location of files in the documentation.

 -- Brian Mays <brian@debian.org>  Mon, 20 Aug 2001 12:04:58 -0400

grap (1.20-2) unstable; urgency=low

  * Fixed configure script for gcc 3.0.  (Closes: Bug#105009)

 -- Brian Mays <brian@debian.org>  Fri, 27 Jul 2001 15:31:16 -0400

grap (1.20-1) unstable; urgency=low

  * New upstream version.
  * Removed "-g" gcc option as per policy.

 -- Brian Mays <brian@debian.org>  Mon,  9 Jul 2001 10:53:53 -0400

grap (1.10-1) unstable; urgency=low

  * New upstream version.
  * Added Build-Depends.

 -- Brian Mays <brian@debian.org>  Wed, 20 Sep 2000 13:37:57 -0400

grap (1.05-1) unstable; urgency=low

  * New upstream version.

 -- Brian Mays <brian@debian.org>  Mon, 10 Apr 2000 22:59:29 -0400

grap (1.02-1) unstable; urgency=low

  * New upstream version.

 -- Brian Mays <brian@debian.org>  Sun, 12 Mar 2000 11:31:50 -0500

grap (1.01-1) unstable; urgency=low

  * Initial release.

 -- Brian Mays <brian@debian.org>  Mon, 14 Feb 2000 20:49:36 -0500
